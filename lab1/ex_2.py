import json
class calculator():
    def __init__(self):
        pass
    
    def Add(self,l):
        output={'operands':l,'command': 'add','result':sum(l)}
        json.dump(output,open("output.json","w"))
        return print(output)
    
    def Sub(self,l):
        res=l[0]
        for i in l[1::]:
            res=res-i
        output={'operands':l,'command': 'sub','result':res}
        json.dump(output,open("output.json","w"))
        return print(output)

    def Mul(self,l):
        res=l[0]
        for i in l[1::]:
            res=res*i
        output={'operands':l,'command':'multiply','result':res}
        json.dump(output,open("output.json","w"))
        return print(output)

    def Div(self,l):
        res=0
        for i in l:
            try:
                res=res/i
            except ZeroDivisionError:
                print("In position {} in the list there is a zero. Can't divide by zero!", l.index(i))
            else:
                output={'operands':[a,b],'command':'','result':res}
                json.dump(output,open("output.json","w"))
                return print(output)


if __name__ == "__main__":
    while True:
        command=input('Avaliable command:\nadd: add: to add the operands and print the JSON\nsub: sub: to subtract the operands and print the JSON\nmul: to multiply the operands and print the JSON\ndiv: to divide the operands and print the JSON\nexit: close program\n')
        c=calculator()
        if command=='add':
            l=input('Write number you want to add separated by a colon. When you finish press enter: ')
            l=l.split(',')
            l=[float(i) for i in l]
            c.Add(l)
        elif command=='sub':
            l=input('Write number you want to sub separated by a colon. When you finish press enter: ')
            l=l.split(',')
            l=[float(i) for i in l]
            c.Sub(l)
        elif command=='mul':
            l=input('Write number you want to mul separated by a colon. When you finish press enter: ')
            l=l.split(',')
            l=[float(i) for i in l]
            c.Mul(l)
        elif command=='div':
            l=input('Write number you want to div separated by a colon. When you finish press enter: ')
            l=l.split(',')
            l=[float(i) for i in l]
            c.Div(l)
        elif command=='exit':
            break
        else:
            print('Command not avaliable')