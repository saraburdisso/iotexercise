import json
class calculator():
    def __init__(self):
        pass
    
    def Add(self,a,b):
        output={'operands':[a,b],'command':'a+b','result':a+b}
        json.dump(output,open("output.json","w"))
        return print(output)
    
    def Sub(self,a,b):
        output={'operands':[a,b],'command':'a-b','result':a-b}
        json.dump(output,open("output.json","w"))
        return print(output)

    def Mul(self,a,b):
        output={'operands':[a,b],'command':'a*b','result':a*b}
        json.dump(output,open("output.json","w"))
        return print(output)

    def Div(self,a,b):
        try:
            res=a/b
        except ZeroDivisionError:
            print("Can't divide by zero!")
        else:
            output={'operands':[a,b],'command':'a/b','result':res}
            json.dump(output,open("output.json","w"))
            return print(output)


if __name__ == "__main__":
    while True:
        command=input('Avaliable command:\nadd: add: to add the operands and print the JSON\nsub: sub: to subtract the operands and print the JSON\nmul: to multiply the operands and print the JSON\ndiv: to divide the operands and print the JSON\nexit: close program\n')
        c=calculator()
        if command=='add':
            n1=float(input('first operand: '))
            n2=float(input('second operand: '))
            c.Add(n1,n2)
        elif command=='sub':
            n1=float(input('first operand: '))
            n2=float(input('second operand: '))
            c.Sub(n1,n2)
        elif command=='mul':
            n1=float(input('first operand: '))
            n2=float(input('second operand: '))
            c.Mul(n1,n2)
        elif command=='div':
            n1=float(input('first operand: '))
            n2=float(input('second operand: '))
            c.Div(n1,n2)
        elif command=='exit':
            break
        else:
            print('Command not avaliable')

#dubbi: 
# quando usare raiseException??
# va bene creare il json come ho fatto?
# va bene mettere che il metodo crei il file json e stampi output oppure bisogna mattere qualche altro metodo per la visualizzazione?
# la classe va bene che non abbia attributi?
# dobbiamo gestire il fatto che il file json venga ogni volta salvato con un nome diverso o va bene che ogni volta sia sovrascritto?