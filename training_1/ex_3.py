if __name__ == "__main__":
    print(f"6+4={6+4}")
    print(f"6-4={6-4}")
    print(f"6/4={6/4}")
    print(f"6//4={6//4}")
    #Floor Division - The division of operands where the result is the quotient in which the digits after the decimal point are removed. But if one of the operands is negative, the result is floored, i.e., rounded away from zero (towards negative infinity)
    #9//2 = 4 and 9.0//2.0 = 4.0, -11//3 = -4, -11.0//3 = -4.0
    print(f"6*4={6*4}")
    print(f"6%4={6%4}") #modulo: 6/4=1 resto 2 6%4=2
    print(f"6<4={6<4}")
    print(f"6>4={6>4}")
    print(f"6<=4={6<=4}")
    print(f"6>=4={6>=4}")
    print(f"6==4={6==4}")