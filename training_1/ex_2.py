if __name__ == "__main__":
    name = 'Sara'
    age = 23
    birthday = '22/01/1997'
    print("My name is %s I'm %d I was born the %s" %(name,age,birthday))
    
    #second way
    day = 22
    month = 1
    year = 1997
    print(f"My name is {name} and I'm {age}, I was born the {day}/{month},{year}")