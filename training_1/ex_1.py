if __name__ == "__main__":
    a='ciao'
    b=1
    c=3.5
    d=3.56789

    #primo metodo
    print('Primo metodo: %s %d %f %f %.3f' %(a,b,c,d,d))

    #secondo metodo
    print(f'Secondo meotdo {a} {b} {c} {d} {d:.3}') #3 è il numero totale di digit che voglio
    #in questo caso avrò solo 2 cifre dopo la virgola