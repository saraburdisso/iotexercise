#vogliamo leggere alcuni contatti che sono salvati in un file json
#il file lo salvo in dizionario che ha come chiave 'contacts'. Il value di questa
#chiave è una lista di dizionari in cui ogni dizionari ha le chiavi 'mail'
# 'surname' e 'name' 

#es fatto con prof

import json #libreria json

class ContactManager():
    def __init__(self,fileName): #quando creiamo l'oggetto gli passiamo il nome del file
        self.fileName = fileName #salviamo il filename
        self.contactsList = [] #vogliamo una funzione che salvi tutti i contatti in questa lista

    def loadContacts(self):
        fp = open(self.fileName) #apriamo il file
        td = json.load(fp) #td è un dizionario in cui carico il file json
        #salvo il contenuto del json file in una var temporanea td che è un dizionario
        print(td) #messo solo per visualizzare td
        for contact in td['contacts']:
            newContact={
                'name': contact['name'], 
                'email':contact['mail'],
                'surname':contact['surname']
                }
            self.contactsList.append(newContact)
    def showContacts(self):
        for contact in self.contactsList:
            print(contact)

if __name__ == "__main__":
    cm=ContactManager('contacts.json')
    #se il file non è nella stessa cartella dello script: cm=ContactManager('../usefulFiles/contacts.json')
    cm.loadContacts()
    cm.showContacts()
    #json.dump(contactsList,open('contacts.json','w')) #per scrivere nel file json