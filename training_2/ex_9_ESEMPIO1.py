#we can access to the attributes of the class without any
#restriction

class Student(object):
    def __init__(self,name,surname,birthday):
        self.name=name
        self.surname=surname
        self.birthday=birthday

if __name__=="__main__":
    s=Student("Mickey","Mouse","16/01/1928") #Create a student
    print(s.name+" "+s.surname) #Print his full name
    s.surname="Duck"#Change the student's surname and print it again
    print(s.name+" "+s.surname)