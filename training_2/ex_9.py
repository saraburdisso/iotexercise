
class Car():
    def __init__(self,name,speed,gear=1):
        self.__name = name
        if speed>250:
            self.__speed = 250
        else:
            self.__speed = speed
        self.gear = gear

    def getname(self):
        return self.__name 

    # def getspeed(self):
    #     return self.speed
    # def getgear(self):
    #     return self.gear

    def change_speed(self,speed): #c
        if self.__speed>250:
            self.__speed = 250
        else:
            self.__speed = speed

    def change_gear(self,marcia):
        if marcia>=1 and marcia<=6:
            if (marcia==self.gear+1 or marcia==self.gear-1):
                self.gear = marcia

 
if __name__ == "__main__":
    car = Car('auto',60)
    print(f"car.getname(): {car.getname()}")
    
    #print(car.__name) #restituisce un errore:  " 'Car' object has no attribute '__name' "
    #print(car.name) # "restituisce un errore: 'Car' object has no attribute 'name' "


    # print(f"car.speed: {car.speed}")
    # print(f"car.gear{car.gear}")

    car.__name='car' #non cambia il nome ma Perchè lo esegue??
    print(f"car.getname(): {car.getname()}")
    print(car.__name) #restituisce errore se prima non metto la riga di codice car.name=... Perchè??
    
    ## Bisogna avere un metodo per cambaire la velocità in modo da controllare che il valore non superi 250?? Perchè se scrivo
    # car.speed=255
    # print(car.speed) #perchè ritorna 255?
    # print(car.getspeed()) #non va bene perchè non viene rieseguito il metodo __init__

    # car.change_speed(60)
    # print(car.speed)
    # car.change_speed(255)
    # print(car.speed)

    # car2=Car('abc',260)
    # print(car.speed)

    # print(car2.gear)
    # car2.change_gear(2)
    # print(car2.gear)
    # car2.change_gear(6)
    # print(car2.gear)
    # car2.change_gear(1)
    # print(car2.gear)