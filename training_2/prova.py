import cherrypy

d = [{"mail": "gaearon@somewhere.com", 
"surname": "Abramov", 
"name": "Dan"}, 
{"mail": "floydophone@somewhere.com", 
"surname": "Hunt", 
"name": "Pete"}, ]

class prova1():
    def __init__(self,i,j):
        self.i=i
        self.j=j

p=prova1(2,3)
print(p.__dict__) #{i:2,j:3}

class prova2():
    def __init__(self,i,j,k):
        self.i=i
        self.j=j
        self.k=k
p=prova2(2,3,4)
print(p.__dict__) #{'i':2,'j':3,'k'}

class prova3():
    C=5
    def __init__(self,i,j,k):
        self.i=i
        self.j=j
        self.k=k
p=prova3(2,3,4)
print(p.__dict__) #{'i':2,'j':3,'k'}

class prova4():
    C=5
    def __init__(self,i,j,k):
        self.i=i
        self.j=j
        self.k=k
    def __repr__(self):
        return "{} {} {}".format(self.i,self.j,self.k)
p=prova4(1,2,3)
print(p)
p.i=10
print(p)