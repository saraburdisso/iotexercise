import json
class NBAStats():
    def __init__(self,filename):
        self.filename = filename
        dic = json.load(open(self.filename))
        self.players =[]
        for player in dic['players']:
            self.players.append(player)#è una lista di dizionari in cui ogni dizionario è un giocatore

    #è piu conveniente avere la fase di caricamente al momento della creazione dell'oggetto NBA
    # def loadfile(self):
    #     fp = open(self.filename)
    #     dic = json.load(fp)
    #     for player in dic['players']:
    #         self.players.append(player)
    #     print(self.players)

    def average_heigth(self):
        av_hgt = 0
        # for hgt in self.players['hgt']:
        #     av_hgt += hgt/39.37 #la divisione serve per la conversione in metri
        # av_hgt = av_hgt/len(self.players)
        #questo ciclo for non va bene perchè self.players è una lista
        for player in self.players:
            av_hgt += player['hgt']/39.37
        av_hgt = av_hgt/len(self.players)
        return av_hgt

    def average_weight(self):
        av_weight = 0
        for player in self.players:
            av_weight += player['weight']/2.205 #la divisione è per fare la conversione
        av_weight = av_weight/len(self.players)
        return av_weight

    def average_age(self):
        av_age = 0
        for player in self.players:
            av_age += (2020-player['born']['year'])
        av_age = av_age/len(self.players)
        return av_age

    def average_ratings(self):
        av_rt = {}
        for player in self.players:
            dic = player['ratings'][0] #metto [0] perchè player['ratings'] è una lista con un dizionario dentro
            for k in dic.keys():
                if k not in av_rt:
                    av_rt[k]=0
                av_rt[k] += dic[k]/len(self.players)
        return av_rt

if __name__ == "__main__":
    nba=NBAStats('playerNBA.json')
    heigth=nba.average_heigth()
    print(heigth)
    weight=nba.average_weight()
    print(weight)
    age = nba.average_age()
    print(age)
    ratings = nba.average_ratings()
    print(ratings)