#52 carte da poker: 13 cuori 13 quadri 13 fiori e 13 picche
#ci sono asso, da 2 a 10, fante, donna e re
import numpy as np
import random

class Deck():
    suits=['Hearts','Diamonds','Clubs','Spades']
    values=['A','2','3','4','5','6','7','8','9','10','J','Q','K']
    def __init__(self):
        self.cards=[ Card(v,s) for v in Deck.values for s in Deck.suits] #ottengo una lista
    
    def shuffle(self):
        random.shuffle(self.cards)
    
    def deal(self,n=1):
        """Returns n cards, 1 if n is not specified"""
        if n<=len(self.cards):
            drawn=[]
            for i in range(n):
                drawn.append(self.cards.pop())
            return drawn
        elif n>len(self.cards) and len(self.cards)!=0:
            return self.deal(len(self.cards))
        else:
            return None


    def __repr__(self):
        return(f'{self.cards}')

class Card():
    def __init__(self,carta,seme):
        self.carta = carta
        self.seme = seme
    def __repr__(self):
        return f"{self.carta} di {self.seme}"


if __name__ == "__main__":
    deck = Deck()
    #print(deck.cards)
    Carta = Card('2','Hearts')
    #print(Carta)
    carta = deck.deal(1)
    print(carta)
    deck.shuffle()
    print(deck) 