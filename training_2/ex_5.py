from ex_4 import ContactManager
import json

#funziona però non uso l'oggetto contact (vedi ex_4_SOL)

class AddressBook():
    def __init__(self,filename):
        self.filename = filename
        self.contactsList = []
        dic = json.load(open(self.filename))
        self.ContactList = []
        for c in dic['contacts']:
            self.ContactList.append(c)

    def show(self):
        for c in self.ContactList:
            print(c)

    def find_by_name(self,name):
        results = []
        for c in self.ContactList:
            if c['name']==name:
                results.append(c)
        print('Results found:')
        [print(f'{i}') for i in results]
    
    def update(self,name,surname):
        for c in self.ContactList:
            if c['name']==name and c['surname']==surname:
                c['mail']=input(f"Insert the new mail of {name} {surname}: ")

    def remove_contact(self,name):
        count=-1
        for c in self.ContactList:
            count+=1
            if c['name']==name:
                self.ContactList.pop(count)

    def add_contact(self,name,surname,mail):
        contact = {'mail': mail,
                   'surname':surname,
                   'name':name,
                   }
        self.ContactList.append(contact)

    def save(self):
        content = {'contacts':[]}
        for c in self.ContactList:
            content['contacts'].append(c)
        fp=open(self.filename,'w')
        json.dump(content,fp)
        fp.close()    


# if __name__ == "__main__":
#     book=AddressBook('contacts.json') #create an object of the class
#     book.show() #show all the contacts
#     print('\n')
#     book.find_by_name('Dan') #find a contact
#     print('\n')
#     book.remove_contact('Dan') #remove a contact according to his name
#     book.show()
#     book.add_contact('Peter','Parker','notspiderman@marvel.com') #add a contact
#     print('\n')
#     book.show()
#     book.save()
#     print('\n')
#     book.show()

if __name__=="__main__":
    book=AddressBook('contacts.json')
    print('Welcome to the application to manage your contacts')
    helpMessage="Press 's' tho show the list of contacts\nPress 'n' to add a contact\nPress 'f' to find a contact\nPress 'd' to delete a contact\nPress 'q'to save end exit"
    while True:
        print(helpMessage)
        command=input()
        if command=='s':
            book.show()
        elif command=='n':
            name=input('Write the name of the contact : ')
            surname=input('Write the surname of the contact : ')
            mail=input('Write the mail of the contact : ')
            book.add_contact(name,surname,mail)
            print('Contact Added')
        elif command=='d':
            name=input('Write the name of the contact you want to delate')
            book.remove_contact(name)
        elif command=='f':
            name=input('Write the name you want to find')
            book.find_by_name(name)
        elif command=='u':
            name=input('Write the name of the contact you want to update: ')
            surname=input('Write the surname of the contact you want to update : ')
            book.update(name,surname)
        elif command=='q':
            book.save()
            break
        else:
            print('command not avaliable')

	