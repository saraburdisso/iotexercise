import math
from ex_1 import Point

class Line():
    def __init__(self,m,q):
        self.m = m
        self.q = q

    def line_from_points(self,point1,point2):
        m = (point2.y - point2.y)/(point2.x - point1.x)
        q = - point1.x * ((point2.y - point1.y)/(point2.x - point1.x) ) + point1.y
        #print(f'y = x*{m} + {q} ') 
        return Line(m,q)

    def distance(self,point):
        d = abs(point.y - (self.m*point.x + self.q))/math.sqrt(1 + self.m**2)
        return d

    def intersection(self,other):
        if self.m==other.m:
            print('le linee sono parallele')
        else:
            x=(other.q-self.q)/(self.m-other.m)
            y=self.m*((other.q-self.q)/(self.m-other.m))+self.q
            return Point(x,y)
    
    def __repr__(self):
        return(f'y = x*{self.m} + {self.q} ')




if __name__ == "__main__":
    l = Line(3,2)
    print(l)
    print(type(l))

    a=Point(0,1)
    b=Point(2,2)
    linea=l.line_from_points(a,b) #mettendo return line(m,n) ottengo un oggetto in uscita
    print(type(linea))
    print(linea)
    l=Line(1,0)
    a=Point(1,5)
    print(l.distance(a))
    m=Line(-1,0)
    i=l.intersection(m)
    print(i)