import math
class Point():
    def __init__(self,x,y):
        super(Point, self).__init__()
        #parte inserita nelle soluzioni ma non messa dal prof
        self.x = x
        self.y = y

    def distance(self,other):
        return math.sqrt((self.x-other.x)**2+(self.y-other.y)**2)

    def move(self,dx,dy):
        self.x +=  dx
        self.y += dy
    
    def __repr__(self):
        return(f'P={self.x},{self.y}')
        #return "{},{}".format(self.x,self.y)

if __name__ == "__main__":
    a = Point(7,1)
    b = Point(1,1)

    print(a.distance(b))
    a.move(2,2)
    print(a.x)
    print(a) #printa la cella di memoria del punto, per non farlo devo aggiungere metodo repr