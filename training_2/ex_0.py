import math
class SquareManager(): #nelle parentesi di solito non si mette nula
    def __init__(self,side): #parametri che voglio dare all'oggetto all'inizio
        self.side = side
    
    def area(self):
        return self.side**2

    def perimeter(self):
        return self.side*4

    def diagonal(self):
        return self.side*math.sqrt(2)

#possiamo scrivere il main direttamente nel file
if __name__ == "__main__":
    sm=SquareManager(3) 
    print(f"The area of the square with side{sm.side}={sm.area()}") 
    print(f"The perimeter of the square with side{sm.side}={sm.perimeter()}") 
    print(f"The diagonal of the square with side{sm.side}={sm.diagonal():.3f}")

#posso anche avere la classe in un file e richiamarla in un main in un altro file