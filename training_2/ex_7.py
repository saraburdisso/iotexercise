import math
class Circle():
    def __init__(self,raggio):
        self.raggio = raggio
    
    def area(self):
        area = math.pi*self.raggio**2
        return area
    
    def perimetro(self):
        perimetro = 2*math.pi*self.raggio
        return perimetro

class Cilinder(Circle):
    def __init__(self,raggio,altezza):
        Circle.__init__(self,raggio)
        self.altezza = altezza
    
    def area(self):
        sup_lat = (Circle(self.raggio).perimetro())*self.altezza
        base = Circle(self.raggio).area()
        return (2*base+sup_lat)

    def volume(self):
        base = Circle(self.raggio).area()
        volume = base*self.altezza
        return volume

if __name__ == "__main__":
    cerchio = Circle(3)
    print(cerchio.perimetro())
    print(cerchio.area())
    cilindro = Cilinder(3,8)
    print(cilindro.area())
    print(cilindro.volume())