class Animal():
    def __init__(self,name):
        self.name=name
    
    def __repr__(self):
        return f"{self.name} is an animal"
    def jump(self):
        print(f"{self.name} has jumpped")

class Quadruped(Animal):
    def __init__(self,name):
        Animal.__init__(self,name)
        self.n_of_legs=4

    def __repr__(self):
        return "{} is a quadruped".format(self.name)

class Dog(Quadruped):
    def __init__(self,name,family):
        Quadruped.__init__(self,name)
        self.family = family
        self.verse = "Woof!"

    def __repr__(self):
        return "{} is a dog of the family {}".format(self.name,self.family)
    def talk(self):
        print(f"{self.name} says: {self.verse}")

if __name__ == "__main__":
    pongo=Animal('Pongo')
    print(pongo)
    pongo.jump()

    pongo=Quadruped('Pongo')
    print(pongo)
    pongo.jump()

    pongo=Dog('Pongo','Dalmata')
    print(pongo)
    print(pongo.n_of_legs)
    pongo.jump()
    pongo.talk()