import cherrypy

class ParamsListe():
    exposed=True

    def __init__(self):
        pass

    def GET(self,**params):
        if params!={}:
            keysList=params.keys()
            #keysList=list(params.keys())
            valuesList=[params[key] for key in params.keys()]
        output=f" Keys : {keysList}, values:{valuesList}"
        return output
    #con get non posso usare body per usare il body bobbiamo cambiare il codice (possiamo usare lo stesso codice di get ma mettendo put e funziona, se vogliamo usare il body dobbiamo modificare il codice per leggere il body)
    #vedi codice ex_2_PUT
    
    #ottengo un erroe su postman se tengo GET quando ho pUT nel codice
    # def PUT(self,**params):
    #     if params!={}:
    #         #keysList=params.keys()
    #         keysList=list(params.keys())
    #         valuesList=[params[key] for key in params.keys()]
    #     output=f" Keys : {keysList}, values:{valuesList}"
    #     return output

if __name__ == "__main__":
    conf={
        '/':{
                'request.dispatch':cherrypy.dispatch.MethodDispatcher(),
                'tool.session.on':True
        }
    }
    #http://127.0.0.1:8080/first/second/third?p1=a&p2=b&p3=c
    #http://localhost:8080/first/second/third?p1=a&p2=b&p3=c
    cherrypy.quickstart(ParamsListe(),'/',conf)