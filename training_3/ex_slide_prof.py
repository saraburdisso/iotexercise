import cherrypy
class HelloWorld(object):
    @cherrypy.expose
    def index(self):
        return "Hello world!"

if __name__ == '__main__':
    cherrypy.tree.mount (HelloWorld())
    cherrypy.engine.start()
    cherrypy.engine.block()