import requests 
import json
# r = requests.get('https://api.github.com/events') 
# r.text #read the content of the server’s response
# r.json() #builtin JSON decoder, in case you’re dealing with JSON data

# #print(r.text)
# #print(r.json())

# r=requests.get('https://api.exchangeratesapi.io/latest?base=EUR')
# #print(r.text)
# r=requests.get('https://api.exchangeratesapi.io/2020-10-26')
# #print(r.text)
# r=requests.get('https://api.exchangeratesapi.io/history?start_at=2019-10-26&end_at=2020-10-26')
# print(r.text)

if __name__ == "__main__":
    while True:
        command = input('Avaliable command\nl: latest change rate\ne: exchange rates of a specific day\nh: historic exchange rates\nquit: q\n')
        if command=='l':
            i=input('Specify the base currency: ')
            payload={'base':i}
            r=requests.get('https://api.exchangeratesapi.io/latest',params=payload)
            print(r.url)
            print(r.text)
        elif command=='e':
            i=input('Specify the day as YYYY-MM-DD: ')
            URL='https://api.exchangeratesapi.io/'+i
            #r=requests.get('https://api.exchangeratesapi.io/',params=i) #mette il ? che non ci deve essere
            r=requests.get(URL)
            print(r.url)
            print(r.text)
        elif command=='h':
            start=input('Specify the period: \nfrom (YYYY-MM-DD) : ')
            end=input('to (YYYY-MM-DD) : ')
            payload={'start_at':start,'end_at':end}
            r=requests.get('https://api.exchangeratesapi.io/history',params=payload)
            print(r.url)
            print(r.text)
        elif command=='q':
            break
        else:
            print('Command not avaliable')