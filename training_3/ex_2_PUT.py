import cherrypy
import json #voglio inviare il body as a json

class ParamsListe():
    exposed=True

    def __init__(self):
        pass

    def PUT(self):
        body=cherrypy.request.body.read() #dalla cherrypy request leggi il body
        #body=cherrypy.request.body.read('utf-8') #con alcuni browser bisogna scrivere questo
        jsonBody=json.loads(body) #trasformo in un dizionario la stringa
        return f" Keys : {list(jsonBody.keys())}, values:{list(jsonBody.values())}"
    #in postmat cliccare, con metodo PUT impostato, su body e selezionare raw e json per avere la sintassi di json

if __name__ == "__main__":
    conf={
        '/':{
                'request.dispatch':cherrypy.dispatch.MethodDispatcher(),
                'tool.session.on':True
        }
    }
    #http://127.0.0.1:8080/first/second/third?p1=a&p2=b&p3=c
    #http://localhost:8080/first/second/third?p1=a&p2=b&p3=c
    cherrypy.quickstart(ParamsListe(),'/',conf)