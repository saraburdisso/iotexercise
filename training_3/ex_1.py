import cherrypy

class UriReverser():
    exposed=True

    def __init__(self,name):
        self.name=name
        #pass

    def GET(self,*uri):
        if len(uri)!=0:
            word=uri[0]
            reversedWord=word[::-1]
            return reversedWord #controllare dove mette il return e come prova il codice
            
if __name__ == "__main__":
    conf={
        '/':{
                'request.dispatch':cherrypy.dispatch.MethodDispatcher(),
                'tool.session.on':True
        }
    }
    #sintassi equivalenti
    #http://127.0.0.1:8080/first/second/third?p1=a&p2=b&p3=c
    #http://localhost:8080/first/second/third?p1=a&p2=b&p3=c
    cherrypy.quickstart(UriReverser('test'),'/',conf)