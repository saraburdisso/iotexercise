import cherrypy
import os

class Example(object):
	"""docstring for Reverser"""
	exposed=True
	def __init__(self):
		self.id=1
	def GET(self):
		return open("index.html")


if __name__ == '__main__':
	conf={
		'/':{
				'request.dispatch':cherrypy.dispatch.MethodDispatcher(),
				'tools.staticdir.root': os.path.abspath(os.getcwd()),
		# 	},
		#  '/css':{
		#  'tools.staticdir.on': True,
		#  'tools.staticdir.dir':'./css'
		#  },
		#  '/js':{
		#  'tools.staticdir.on': True,
		#  'tools.staticdir.dir':'./js'
		},
	}		
	#se commento la parte per file css
	#ogni che volta che la pagina ha bisogno di file nel server dobbiamo dire a cherrypy dove sono i file

	#altro modo per lanciare il server
	#cherrypy is a blocking function so if, after cherry.quickstart, we write some line of code
	#thsese line wont be executed
	#With this way of writing we can write other lines of code and they will be executed, the script wont be locked at line cherrypy.quickstart
	cherrypy.tree.mount(Example(),'/',conf)
	cherrypy.engine.start()
	cherrypy.engine.block()
