import cherrypy


#è un semplice server che printa tutto qeullo che è scritto nell'URL
#uri è una tupla, per qiesto scrivo uri=list(uri)
class HelloWorld(object):
    exposed=True
    def GET(self,*uri,**params):
        #Standard output
        output="Hello World"
        #Check the uri in the requests
        #<br> is just used to append the content in a new line
        #(<br> is the \n for HTML)
        uri=list(uri) #riga di codice che potrebbe essere necessaria
        if len(uri)!=0: 
            output+='<br>uri: '+','.join(uri)
            #Check the parameters in the request
            #<br> is just used to append the content in a new line
            #(<br> is the \n for HTML)
        if params!={}: 
            output+='<br>params: '+str(params)
        return output
if __name__ == "__main__":
    conf={
        '/':{
                'request.dispatch':cherrypy.dispatch.MethodDispatcher(),
                'tool.session.on':True
        }
    }
    cherrypy.quickstart(HelloWorld(),'/',conf)
    #all'URL che ho in output trovo il server che ho appena coded
    #se non ho uri o param mi viene stampato Hello World
    #se nella barra sul browser aggiungo uri (ho una lista con le parole 
    # separate da / nell'uri ) o param